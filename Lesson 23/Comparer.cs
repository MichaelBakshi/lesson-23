﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class ComparerByProtection : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return y.Protection - x.Protection;
        }
    }

    class ComparerByPurchases: IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return y.TotalPurchases - x.TotalPurchases;
        }
    }

    class ComparerByBirthYear: IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.BirthYear - y.BirthYear;
        }
    }
}
