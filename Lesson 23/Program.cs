﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer customer1 = new Customer(123, "Aharon", 1950, "Arkansas", 1, 7);
            Customer customer2 = new Customer(456, "Bob", 1960, "Boston", 2, 3);
            Customer customer3 = new Customer(789, "Charlie", 1970, "Chicago", 3,0);

            MyQueue myQueue = new MyQueue();

            myQueue.Enqueue(customer1);
            myQueue.Enqueue(customer2);
            myQueue.Enqueue(customer3);

            //myQueue.Dequeue();
            myQueue.Dequeue();

            Console.WriteLine("Next customer in line is:");
            Console.WriteLine(myQueue.WhoIsNext());

            Console.WriteLine("Sorted by protection, highest protection first");
            myQueue.SortByProtection();
            foreach(var item in myQueue.customers)
            {
                Console.WriteLine(item.Name);
            }

            Console.WriteLine("Sorted by purchases, most purchases first ");
            myQueue.SortByTotalPurchases();
            foreach (var item in myQueue.customers)
            {
                Console.WriteLine(item.Name);
            }

            Console.WriteLine("Sorted by year of birth, the oldest goes first:");
            myQueue.SortByBirthyear();
            foreach (var item in myQueue.customers)
            {
                Console.WriteLine(item.Name);
            }


            //SelfImplementStack myStack = new SelfImplementStack();
            //myStack.Push(1);  // [1]
            //myStack.Push(3);  // [3, 1]
            //myStack.Push(-4); // [-4, 3, 1]
            //int z1 = myStack.Pop();  // z1 = -4. [3, 1]
            //int z2 = myStack.Peek(); // z2 =  3. [3, 1]
            //myStack.Pop(); // [1]
            //myStack.Pop(); // []
            //myStack.Pop(); // []

        }
    }
}
