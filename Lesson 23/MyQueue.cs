﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class MyQueue
    {
        private List<Customer> _customers;
        public List<Customer> customers { get { return _customers; } set { _customers = value; } }
        public MyQueue()
        {
            customers = new List<Customer>();
        }

        public void Enqueue(Customer customer)
        {
            customers.Add(customer);
        }
        //public void Push(int x)
        //{
        //    _items.Insert(0, x);
        //}

        public Customer Dequeue()
        {
            if (customers.Count == 0)
                return customers[0];

            Customer customerWhoGotService = customers[0];
            customers.RemoveAt(0);
            return customerWhoGotService;
        }
        //public int Pop()
        //{
        //    if (_items.Count == 0)
        //       return 0;
        //    int result = _items[0];
        //   _items.RemoveAt(0);
        //    return result;
        //}


        public void Init(List<Customer> customers)
        {
            this.customers = customers;
        }

        public void Clear()
        {
            customers.Clear();
        }

        public Customer WhoIsNext()
        {
            return customers[0];
        }

        public int Count 
        { 
            get 
            {
                return customers.Count;
            }
        }
        
        public void SortByProtection()
        {
            customers.Sort(new ComparerByProtection());                     
        }

        public void SortByTotalPurchases()
        {
            customers.Sort(new ComparerByPurchases());
        }

        public void SortByBirthyear()
        {
            customers.Sort(new ComparerByBirthYear());
        }

        public List<Customer>  DequeueCustomers(int x)
        {
            return customers;
        }

        public void AniRakSheela(Customer customer)
        {

        }

        public Customer  DequeueProtectiza()
        {
            return customers[0];
        }

        public override string ToString()
        {
             return customers.ToString();
                
        }

    }
}
