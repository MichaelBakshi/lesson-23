﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public string Address { get; set; }
        public int Protection { get; set; }
        public int TotalPurchases { get; set; }
        
        public Customer(int id,string name,int birthyear,string address,int protection, int totalPurchases)
        {
            this.Id = id;
            this.Name = name;
            this.BirthYear = birthyear;
            this.Address = address;
            this.Protection = protection;
            this.TotalPurchases = totalPurchases;
        }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }
}
